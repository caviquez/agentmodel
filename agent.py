"""
Instituto Tecnologico de Costa Rica
Escuela de compiutacion
Curso: Inteligencia Artificial
Profesor: Esteban Arias Mendez
Estudiantes:
Camila Viquez Alpizar 2014152891
Fabricio Alvarado Esquivel 2016089643
"""

import random
import time
position = []
matrix = []

def readFile():
    global position
    global matrix
    with open("text.txt") as fp:
        line = fp.readline()
        position = line.split()
        position = [int(i) for i in position]
        for line in fp:
            char = [char for char in line]
            matrix.append(char)
        last = len(matrix)-1
        matrix[last].append('\r')
        matrix[last].append('\n')


def createMetrix():
    readFile()
    row = len(matrix)
    column = len(matrix[0])-1
    print("Matriz leida: "+str(row)+"x"+str(column))
    for i in range(row):
        for j in range(column):
            matrix[i][j]=[item.replace('\r',' ') for item in matrix[i][j]]
            matrix[i][j]=[item.replace('\t',' ') for item in matrix[i][j]]
            matrix[i][j]=[item.replace('\n',' ') for item in matrix[i][j]]
        format = ' '.join(str(i) for i in matrix[i])
        print(format)
    matrix[position[0]][position[1]] = ['A']

def printMatrix():
    row = len(matrix)
    column = len(matrix[0])-1
    print("Matriz actual:")
    for i in range(row):
        format = ' '.join(str(i) for i in matrix[i])
        print(format)

def positionAgent(row, column):
    if (matrix[row][column] == [' ']):
        matrix[row][column] = ['A']
        position[0] = row
        position[1] = column
        return True
    else:
        return False

def moveUp():
    if (validateMove(position[0]-1, position[1])):
        if (positionAgent(position[0]-1, position[1])):
            matrix[position[0]+1][position[1]] = [' ']
            printMatrix()
            time.sleep(5)
            return True
    return False

def moveRight():
    if (validateMove(position[0], position[1]+1)):
        if (positionAgent(position[0], position[1]+1)):
            matrix[position[0]][position[1]-1] = [' ']
            printMatrix()
            time.sleep(5)
            return True
    return False

def moveDown():
    if (validateMove(position[0]+1, position[1])):
        if (positionAgent(position[0]+1, position[1])):
            matrix[position[0]-1][position[1]] = [' ']
            printMatrix()
            time.sleep(5)
            return True
    return False

def moveLeft():
    if (validateMove(position[0], position[1]-1)):
        if (positionAgent(position[0], position[1]-1)):
            matrix[position[0]][position[1]+1] = [' ']
            printMatrix()
            time.sleep(5)
            return True
    return False

def moveUpRight():
    if (validateMove(position[0]-1, position[1]+1)):
        if (positionAgent(position[0]-1, position[1]+1)):
            matrix[position[0]+1][position[1]-1] = [' ']
            printMatrix()
            time.sleep(5)
            return True
    return False

def moveDownRight():
    if (validateMove(position[0]+1, position[1]+1)):
        if (positionAgent(position[0]+1, position[1]+1)):
            matrix[position[0]-1][position[1]-1] = [' ']
            printMatrix()
            time.sleep(5)
            return True
    return False

def moveDownLeft():
    if (validateMove(position[0]+1, position[1]-1)):
        if (positionAgent(position[0]+1, position[1]-1)):
            matrix[position[0]-1][position[1]+1] = [' ']
            printMatrix()
            time.sleep(5)
            return True
    return False

def moveUpLeft():
    if (validateMove(position[0]-1, position[1]-1)):
        if (positionAgent(position[0]-1, position[1]-1)):
            matrix[position[0]+1][position[1]+1] = [' ']
            printMatrix()
            time.sleep(5)
            return True
    return False

def validateMove(row, column):
    if (row >= 0 and row < len(matrix)):
        if (column >= 0 and column < len(matrix[0])):
            if (matrix[row][column] == [' ']):
                return True
    return False

def evaluateClockMove():
    positions = [[position[0]-1, position[1]],
                [position[0]-1, position[1]-1],
                [position[0], position[1]-1],
                [position[0]+1, position[1]-1],
                [position[0]+1, position[1]],
                [position[0]+1, position[1]+1],
                [position[0], position[1]+1],
                [position[0]-1, position[1]+1]]
    counter = 0
    for pos in positions:
        if (validateMove(pos[0], pos[1])):
            return counter
        counter += 1

def evaluateCrossMove():
    positions = [[position[0]-1, position[1]-1],
                [position[0]+1, position[1]-1],
                [position[0]+1, position[1]+1],
                [position[0]-1, position[1]+1],
                [position[0]-1, position[1]],
                [position[0], position[1]-1],
                [position[0]+1, position[1]],
                [position[0], position[1]+1]]
    counter = 0
    for pos in positions:
        if (validateMove(pos[0], pos[1])):
            return counter
        counter += 1

def evaluateRandomMove():
    positions = [[position[0]-1, position[1]-1],
                [position[0]+1, position[1]-1],
                [position[0]+1, position[1]+1],
                [position[0]-1, position[1]+1],
                [position[0]-1, position[1]],
                [position[0], position[1]-1],
                [position[0]+1, position[1]],
                [position[0], position[1]+1]]
    random.shuffle(positions)
    counter = 0
    for pos in positions:
        if (validateMove(pos[0], pos[1])):
            return counter
        counter += 1

def clockMove(move):
    moves = [moveUp(), moveUpLeft(), moveLeft(), moveDownLeft(),
            moveDown(), moveDownRight(), moveRight(), moveUpRight()]
    moves[move]

def crossMove(move):
    moves = [moveUpLeft(), moveDownLeft(), moveDownRight(), moveUpRight(),
            moveUp(), moveRight(), moveDown(), moveLeft()]
    moves[move]

def randomMove(move):
    moves = [moveUpLeft(), moveDownLeft(), moveDownRight(), moveUpRight(),
            moveUp(), moveRight(), moveDown(), moveLeft()]
    random.shuffle(moves)
    moves[move]

def moveAgent():
    while(True):
        clock = evaluateClockMove()
        cross = evaluateCrossMove()
        random = evaluateRandomMove()
        if (clock < cross and clock < random):
            print("Clock move picked")
            clockMove(clock)
        elif (random < cross):
            print("Random move picked")
            randomMove(random)
        else:
            print("Cross move picked")
            crossMove(cross)

createMetrix()
print(evaluateClockMove())
moveAgent()
